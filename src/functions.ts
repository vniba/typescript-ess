function sum(n1: number, n2: number): number {
  return n1 + n2;
}

console.log(sum(20, 94));

function greet(str: string): void {
  console.log(`welcome ${str}`);
}
greet("soda");

let comVal: (n: number, n1: number) => number;
let strs: (n: string) => void;
comVal = sum;
strs = greet;
console.log(comVal(20, 30));
strs("do the");
