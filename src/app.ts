let userInput: unknown;
let userName: string;
userInput = "jo";
if (typeof userInput === "string") userName = userInput;

//never
function genErr(msg: string, code: number): never {
  throw { message: msg, errorCode: code };
}

const er = genErr("noway", 33);
console.log(er);
