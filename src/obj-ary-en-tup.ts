// const ADMIN = 0;
// const READ_ONLY=1;
// const AUTHOR = 2;
enum Role {
  ADMIN = 8,
  READ_ONLY,
  AUTHOR,
}

const person: {
  name: string;
  age: number;
  hobbies: string[];
  role: [number, string];
  privilege: {};
} = {
  name: "jon",
  age: 30,
  hobbies: ["gym", "swimming"],
  role: [2, "author"],
  privilege: Role.ADMIN,
};
let favAct: string[];
favAct = ["killing"];

person.role.push(3);
console.log(person);

for (const hobby of person.hobbies) {
  console.log(hobby.toUpperCase());
}
