//number, string, boolean, object, array,enum,tuple
type Combinable = "num" | "str";

function combine(
  input1: number | string,
  input2: number | string,
  resultT: Combinable
) {
  let result;
  if (
    (typeof input1 === "number" && typeof input2 === "number") ||
    resultT === "num"
  ) {
    result = +input2 + +input2;
  }
  if (resultT === "str") {
    result = input1.toString() + input2.toString();
  }
  return result;
  // if (resultT === "num") {
  //   return +result;
  // } else {
  //   return result.toString();
  // }
}
console.log(combine(3, 3, "num"));
console.log(combine("son", "nos", "str"));
console.log(combine(1, 1, "str"));
