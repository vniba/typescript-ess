"use strict";
var Role;
(function (Role) {
    Role[Role["ADMIN"] = 8] = "ADMIN";
    Role[Role["READ_ONLY"] = 9] = "READ_ONLY";
    Role[Role["AUTHOR"] = 10] = "AUTHOR";
})(Role || (Role = {}));
const person = {
    name: "jon",
    age: 30,
    hobbies: ["gym", "swimming"],
    role: [2, "author"],
    privilege: Role.ADMIN,
};
let favAct;
favAct = ["killing"];
person.role.push(3);
console.log(person);
for (const hobby of person.hobbies) {
    console.log(hobby.toUpperCase());
}
//# sourceMappingURL=obj-ary-en-tup.js.map