"use strict";
function combine(input1, input2, resultT) {
    let result;
    if ((typeof input1 === "number" && typeof input2 === "number") ||
        resultT === "num") {
        result = +input2 + +input2;
    }
    if (resultT === "str") {
        result = input1.toString() + input2.toString();
    }
    return result;
}
console.log(combine(3, 3, "num"));
console.log(combine("son", "nos", "str"));
console.log(combine(1, 1, "str"));
//# sourceMappingURL=uni-lite-alias.js.map